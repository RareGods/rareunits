program Project9;

{$APPTYPE CONSOLE}
{$R *.res}

uses
  System.SysUtils,
  RareUnit.HostManager in 'RareUnit.HostManager.pas';

procedure Test;
var
  x: THostRecord;
begin
  x := THostRecord.Create
    (' 127.0.0.1 scott1.scottro.net scott1 localhost.localdomain localhost. # source server');
  try
    Writeln(x.ToString);
    x.Enabled := True;
    Writeln(x.ToString);
    x.Enabled := False;
    Writeln(x.ToString);
    x.Enabled := True;
    Writeln(x.ToString);
  finally
    x.Free;
  end;
end;

begin
  try
    { TODO -oUser -cConsole Main : Insert code here }
    Test;
    Readln;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;

end.

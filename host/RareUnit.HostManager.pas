unit RareUnit.HostManager;

interface

uses
  System.Classes,
  System.Generics.Collections;

type
  THostRecord = class
  private
    FComment: string;
    FIP: string;
    FDomains: TArray<string>;
    FEnabled: Boolean;
    FDomain: string;
    function GetEnabled: Boolean;
    procedure SetEnabled(const Value: Boolean);
  public
    constructor Create(ALine: string);
    function ToString: string; override;
    procedure FromString(ALine: string);
    property Enabled: Boolean read GetEnabled write SetEnabled;
    property Comment: string read FComment write FComment;
    property IP: string read FIP write FIP;
    property Domains: TArray<string> read FDomains write FDomains;
    property Domain: string read FDomain write FDomain;
  end;

  THostManager = class
  private
    FRecords: TObjectList<THostRecord>;
    FHostFile: TStringList;
  public
    constructor Create;

    procedure LoadFromFile(const AFilename: string);
    destructor Destroy; override;
  end;

implementation

uses
  System.SysUtils;

{ THostRecord }

constructor THostRecord.Create(ALine: string);
begin
  FromString(ALine);
end;

procedure THostRecord.FromString(ALine: string);
var
  i: integer;
  Payload: string;
begin
  Payload := ALine.Trim;
  i := Payload.IndexOf('#');
  if i > -1 then // have comment
  begin
    Comment := Payload.Substring(i);
    FEnabled := i > 0;
  end;
  // not only comment
  begin
    if i = -1 then
      i := Payload.Length
    else
      Payload := Payload.Substring(0, i);
    i := Payload.IndexOf(' ');
    IP := Payload.Substring(0, i);
    Domain := Payload.Substring(i + 1);
    Domains := Domain.Split([' '], TStringSplitOptions.ExcludeEmpty);
  end;
end;

function THostRecord.GetEnabled: Boolean;
begin
  Result := FEnabled;
end;

procedure THostRecord.SetEnabled(const Value: Boolean);
begin
  if Value = FEnabled then
    Exit;
  if Value then
    FromString(Comment.Substring(1))
  else
    FromString('#' + Self.ToString);
end;

function THostRecord.ToString: string;
begin
  Result := IP + ' ' + string.Join(' ', Domains) + ' ' + Comment;
end;

{ THostManager }

constructor THostManager.Create;
begin
  FRecords := TObjectList<THostRecord>.Create();
  FHostFile := TStringList.Create;
end;

destructor THostManager.Destroy;
begin
  FHostFile.Free;
  FRecords.Free;
  inherited;
end;

procedure THostManager.LoadFromFile(const AFilename: string);
var
  i: integer;
begin
  FHostFile.LoadFromFile(AFilename);
  FRecords.Clear;
  for i := 0 to FHostFile.Count - 1 do
    FRecords.Add(THostRecord.Create(FHostFile[i]));
end;

end.

unit RareUtils.ReTry;

interface

uses
  System.SysUtils;

type
  TReTry = class
  private
    FTryMaxCount: Integer;
    FTryCurrentCount: Integer;
    FActive: Boolean;
    FOnError: TProc<Exception>;
  protected
    procedure DoOnError(E: Exception);
  public
    procedure Proc(AProc: TProc); overload;
    function Func<T>(AFunc: TFunc<T>): T; overload;
    class procedure Proc(AProc: TProc; AMax: Integer); overload;
    class function Func<T>(AFunc: TFunc<T>; AMax: Integer): T; overload;
    class function Func<T>(AFunc: TFunc<T>; AMax: Integer; AActive: TFunc<Exception, Boolean>): T; overload;
    procedure AfterConstruction; override;
    property Active: Boolean read FActive write FActive;
    property TryMaxCount: Integer read FTryMaxCount write FTryMaxCount default 5;
    property TryCurrentCount: Integer read FTryCurrentCount write FTryCurrentCount default 0;
    property OnError: TProc<Exception> read FOnError write FOnError;
  end;

implementation

{ TReTry }

procedure TReTry.AfterConstruction;
begin
  inherited AfterConstruction;
  TryMaxCount := 5;
  TryCurrentCount := 0;
  Active := True;
end;

procedure TReTry.DoOnError(E: Exception);
begin
  if Assigned(OnError) then
    OnError(E);
end;

class function TReTry.Func<T>(AFunc: TFunc<T>; AMax: Integer; AActive: TFunc<Exception, Boolean>): T;
var
  LSelf: TReTry;
begin
  LSelf := TReTry.Create;
  try
    LSelf.TryMaxCount := AMax;
    LSelf.OnError :=
      procedure(E: Exception)
      begin
        LSelf.Active := AActive(E);
      end;
    Result := LSelf.Func<T>(AFunc);
  finally
    LSelf.Free;
  end;
end;

class function TReTry.Func<T>(AFunc: TFunc<T>; AMax: Integer): T;
begin
  Result := TReTry.Func<T>(AFunc, AMax, nil);
end;

class procedure TReTry.Proc(AProc: TProc; AMax: Integer);
var
  LSelf: TReTry;
begin
  LSelf := TReTry.Create;
  try
    LSelf.TryMaxCount := AMax;
    LSelf.Proc(AProc);
  finally
    LSelf.Free;
  end;
end;

function TReTry.Func<T>(AFunc: TFunc<T>): T;
begin
  if TryCurrentCount > TryMaxCount then
    FActive := False;
  if not FActive then
    Exit;
  try
    Result := AFunc();
    TryCurrentCount := 0;
  except
    on E: Exception do
    begin
      Inc(FTryCurrentCount);
      DoOnError(E);
      Result := Self.Func(AFunc);
    end;
  end;
end;

procedure TReTry.Proc(AProc: TProc);
begin
  Self.Func<Boolean>(
    function: Boolean
    begin
      Result := True;
      AProc;
    end);
end;

end.


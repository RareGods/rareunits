unit UITools.ViewNavigator;

interface

uses
  FMX.Controls,
  System.Rtti,
  System.Generics.Collections,
  System.Classes;

type
  ViewControlAtttribute = class(TCustomAttribute)
  private
    FName: string;
  public
    constructor Create(const AName: string);
    property Name: string read FName write FName;
  end;

  THistory = class
  private
    type
      THistoryItem = record
        Name: string;
        Data: TValue;
        constructor Create(const APage: string; AData: TValue);
      end;
  private
    FHistory: TStack<THistoryItem>;
  public
    procedure Navigate(const APage: string; AData: TValue);
    function Current: THistoryItem;
    function CanBack: Boolean;
    function Back: THistoryItem;
    constructor Create;
    destructor Destroy; override;
  end;

  TOnChangePage = procedure(Sender: TObject; const APageName: string; AData: TValue) of object;

  TViewNavigator = class(TControl)
  private
    FViews: TDictionary<string, TControl>;
    FHistory: THistory;
    FOnChangePage: TOnChangePage;
    function GetView(const Name: string): TControl;
    procedure SetView(const Name: string; const Value: TControl);
  protected
    function DoChangeView(const AName: string; AData: TValue): Boolean;
  public
    procedure FindByAttribute; deprecated;
    function ViewIsRegistered(const AName: string): Boolean;
    procedure AddView(const AName: string; AView: TControl);
    function Navigate(const AName: string; AData: TValue): Boolean;
    function Back: Boolean;
    function CanBack: Boolean;
    procedure FreeAllViews;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property View[const Name: string]: TControl read GetView write SetView;
  published
    property OnChangePage: TOnChangePage read FOnChangePage write FOnChangePage;
  end;

implementation

uses
  FMX.Types,
  System.SysUtils;

const
  C_BACK_TAG = '{1029EC6B-0B4D-4242-A8D9-C7A50C2A292C}';
  { THistory }

function THistory.Back: THistoryItem;
begin
  if CanBack then
    Result := FHistory.Pop;
end;

function THistory.CanBack: Boolean;
begin
  Result := FHistory.Count > 1;
end;

constructor THistory.Create;
begin
  FHistory := TStack<THistoryItem>.Create;
end;

function THistory.Current: THistoryItem;
begin
  if FHistory.Count > 0 then
    Result := FHistory.Peek
end;

destructor THistory.Destroy;
begin
  FHistory.Free;
  inherited;
end;

procedure THistory.Navigate(const APage: string; AData: TValue);
begin
  if (FHistory.Count = 0) or (Current.Name <> APage) then
    FHistory.Push(THistory.THistoryItem.Create(APage, AData));
end;

{ TViewNavigator }

procedure TViewNavigator.AddView(const AName: string; AView: TControl);
begin
  FViews.AddOrSetValue(AName, AView);
end;

function TViewNavigator.DoChangeView(const AName: string; AData: TValue): Boolean;
begin
  if AName = C_BACK_TAG then
    Result := FHistory.CanBack
  else
    Result := ViewIsRegistered(AName);
  if not Result then
    Exit;
  if not FHistory.Current.Name.IsEmpty then
  begin
    FViews[FHistory.Current.Name].Parent := nil;
    FViews[FHistory.Current.Name].Visible := False;
  end;
  if AName = C_BACK_TAG then
    FHistory.Back
  else
    FHistory.Navigate(AName, AData);
  FViews[FHistory.Current.Name].Parent := Self;
  FViews[FHistory.Current.Name].Visible := True;
  FViews[FHistory.Current.Name].Align := TAlignLayout.Client;
  if Assigned(OnChangePage) then
    OnChangePage(Self, FHistory.Current.Name, FHistory.Current.Data);
end;

procedure TViewNavigator.FindByAttribute;
var
  Ctx: TRttiContext;
  LRttiType: TRttiType;
  LAttribute: TCustomAttribute;
begin
  Ctx := TRttiContext.Create;
  try
    for LRttiType in Ctx.GetTypes do
    begin
      for LAttribute in LRttiType.GetAttributes do
      begin
        if LAttribute is ViewControlAtttribute then
          Self.AddView(ViewControlAtttribute(LAttribute).Name, nil);
      end;
    end;

  finally
    Ctx.Free;
  end;
end;

procedure TViewNavigator.FreeAllViews;
var
  LView: TObject;
begin
  for LView in FViews.Values.ToArray do
  begin
    LView.Free;
  end;
end;

function TViewNavigator.Back: Boolean;
begin
  Result := DoChangeView(C_BACK_TAG, nil);
end;

function TViewNavigator.CanBack: Boolean;
begin
  Result := FHistory.CanBack;
end;

constructor TViewNavigator.Create(AOwner: TComponent);
begin
  inherited;
  FViews := TDictionary<string, TControl>.Create;
  Align := TAlignLayout.Client;
  FHistory := THistory.Create;
  if AOwner is TFmxObject then
    Self.Parent := AOwner as TFmxObject;
end;

destructor TViewNavigator.Destroy;
begin
  FViews.Free;
  FHistory.Free;
  inherited;
end;

function TViewNavigator.GetView(const Name: string): TControl;
begin
  Result := FViews[Name];
end;

function TViewNavigator.Navigate(const AName: string; AData: TValue): Boolean;
begin
  Result := DoChangeView(AName, AData);
end;

procedure TViewNavigator.SetView(const Name: string; const Value: TControl);
begin
  FViews.AddOrSetValue(Name, Value);
end;

function TViewNavigator.ViewIsRegistered(const AName: string): Boolean;
begin
  Result := FViews.ContainsKey(AName);
end;

{ ViewControlAtttribute }

constructor ViewControlAtttribute.Create(const AName: string);
begin
  FName := AName;
end;

{ THistory.THistoryItem }

constructor THistory.THistoryItem.Create(const APage: string; AData: TValue);
begin
  Name := APage;
  Data := AData;
end;

end.


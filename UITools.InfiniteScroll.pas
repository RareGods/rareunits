unit UITools.InfiniteScroll;

interface

uses
  System.SysUtils;

type
  ENeedChangeProp = class(Exception)
  private
    FMessage: string;
    procedure SetMessage(const Value: string);
  public
    constructor Create(const Msg: string);
    property Message: string read FMessage write SetMessage;
  end;

  TInfiniteScroll<T> = class
  private
    FCurrentItem: Integer;
    FTotalItems: Integer;
    FLoadAfterPercent: Integer;
    FOnSetLoader: TFunc<Integer, Integer, TArray<T>>;
    FOnStartLoading: TProc;
    FOnEndLoading: TProc<TArray<T>>;
    FActive: Boolean;
    FInProcess: Boolean;
    procedure SetActive(const Value: Boolean);
  protected
    function DoCheckNeedLoading: Boolean;
    procedure DoNeedLoading;
    procedure DoRun;
  public
    procedure AfterConstruction; override;
    property Active: Boolean read FActive write SetActive;
    procedure SetTotalItems(const AValue: Integer);
    procedure SetCurrentItem(const AValue: Integer);
    property LoadAfterPercent: Integer read FLoadAfterPercent write FLoadAfterPercent default 70;
    property OnSetLoader: TFunc<Integer, Integer, TArray<T>> read FOnSetLoader write FOnSetLoader;
    property OnStartLoading: TProc read FOnStartLoading write FOnStartLoading;
    property OnEndLoading: TProc<TArray<T>> read FOnEndLoading write FOnEndLoading;
  end;

implementation

uses
  FMX.Types,
  System.Classes,
  System.Threading;

{ TInfiniteScroll }

procedure TInfiniteScroll<T>.AfterConstruction;
begin
  inherited;
  FCurrentItem := 0;
  FTotalItems := 0;
  FLoadAfterPercent := 70;
  FInProcess := False;
end;

function TInfiniteScroll<T>.DoCheckNeedLoading: Boolean;
begin
  if FInProcess then
    Exit(False);
  if FTotalItems < 1 then
    Exit(True);
  Result := (FTotalItems - FCurrentItem) < 20 //(FCurrentItem / FTotalItems * 100) >= FLoadAfterPercent;
end;

procedure TInfiniteScroll<T>.DoNeedLoading;
begin
  if DoCheckNeedLoading and (not FInProcess) then
  begin
    DoRun;
  end;
end;

procedure TInfiniteScroll<T>.DoRun;
begin
  if Assigned(OnStartLoading) then
    OnStartLoading();
  FInProcess := True;
  TTask.Run(
    procedure
    var
      LReturn: TArray<T>;
    begin
      Log.d('����� ���������');
      LReturn := FOnSetLoader(FTotalItems, 5);
      Log.d('������� ���������');
      TThread.Synchronize(nil,
        procedure
        begin
          Log.d('����� ����������');
          if Assigned(OnEndLoading) then
            OnEndLoading(LReturn);
          FInProcess := False;
          Log.d('������� ����������')
        end);
    end);

end;

procedure TInfiniteScroll<T>.SetActive(const Value: Boolean);
begin
  if FCurrentItem = -1 then
    raise ENeedChangeProp.Create('CurrentItem');
  if FTotalItems = -1 then
    raise ENeedChangeProp.Create('TotalItems');
  if FLoadAfterPercent = -1 then
    raise ENeedChangeProp.Create('LoadAfterPercent');
  if not Assigned(OnSetLoader) then
    raise ENeedChangeProp.Create('OnSetLoader');

  FActive := Value;
end;

procedure TInfiniteScroll<T>.SetCurrentItem(const AValue: Integer);
begin
  FCurrentItem := AValue;
  DoNeedLoading;
end;

procedure TInfiniteScroll<T>.SetTotalItems(const AValue: Integer);
begin
  FTotalItems := AValue;
  DoNeedLoading;
end;

{ ENeedChangeProp }

constructor ENeedChangeProp.Create(const Msg: string);
begin
  Message := Msg;
end;

procedure ENeedChangeProp.SetMessage(const Value: string);
begin
  inherited Message := 'Setup this property: ' + Value;
end;

end.


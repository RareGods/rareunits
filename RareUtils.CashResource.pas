unit RareUtils.CashResource;

interface

uses
  System.SysUtils;

type
  ICashResource = interface
    ['{27C4D7F6-29CF-44E0-89A9-5B3D6628470F}']
    function Extract(const AUrl: string): string;
  end;

  TWebCash = class(TInterfacedObject, ICashResource)
  strict private
    FPath: string;
  private
    function GetPath: string;
    procedure SetPath(const Value: string);
  protected
    function SaveInCash(const AUrl: string): string;
  public
    procedure Delete(const AName: string);
    procedure Download(const AUrl: string; AOnDone: TProc<string>);
    function contains(const AName: string): Boolean;
    function UrlToFileName(const AUrl: string): string;
    function Extract(const AUrl: string): string;
    procedure CleanOldFiles(const ALastDate: TDateTime);
    property Path: string read GetPath write SetPath;
  end;

implementation

uses
  System.Net.HttpClient,
  System.Types,
  System.Classes,
  System.IoUtils;

type
  TPathX = class
    class function Normaliz(const APath: string): string;
  end;
  { TDirCash }

procedure TWebCash.CleanOldFiles(const ALastDate: TDateTime);
var
  AFiles: TStringDynArray;
  ACurrentFile: string;
begin
  AFiles := TDirectory.GetFiles(Path);
  for ACurrentFile in AFiles do
    if ALastDate > TFile.GetLastAccessTimeUtc(ACurrentFile) then
      TFile.Delete(ACurrentFile);
end;

function TWebCash.contains(const AName: string): Boolean;
begin
  Result := FileExists(TPathX.Normaliz(Path + '/' + AName));
end;

procedure TWebCash.Delete(const AName: string);
begin
  TFile.Delete(AName);
end;

procedure TWebCash.Download(const AUrl: string; AOnDone: TProc<string>);
begin
  with TThread.CreateAnonymousThread(
    procedure
    var
      LFileName: string;
    begin
      LFileName := Self.SaveInCash(AUrl);
      TThread.Synchronize(nil,
        procedure
        begin
          AOnDone(LFileName);
        end);
    end) do
  begin
    FreeOnTerminate := True;
    Start;
  end;
end;

function TWebCash.Extract(const AUrl: string): string;
var
  LUrlToFileName: string;
begin
  LUrlToFileName := TPath.Combine(Path, UrlToFileName(AUrl));
  if TFile.Exists(LUrlToFileName) then
    Result := LUrlToFileName
  else
    Result := SaveInCash(AUrl);
  TFile.SetLastAccessTime(Result, Now);
end;

function TWebCash.GetPath: string;
begin
  Result := FPath;
end;

function TWebCash.SaveInCash(const AUrl: string): string;
var
  LHttp: THTTPClient;
  LDiskFile: TFileStream;
  LResponse: IHTTPResponse;
begin
  LHttp := THTTPClient.Create;
  Result := UrlToFileName(AUrl);
  LDiskFile := TFileStream.Create(Result, fmCreate);
  try
    LResponse := LHttp.Get(AUrl);
    LDiskFile.CopyFrom(LResponse.ContentStream, LResponse.ContentStream.Size);
  finally
    LDiskFile.Free;
    LHttp.Free;
  end;
end;

procedure TWebCash.SetPath(const Value: string);
begin
  FPath := TPathX.Normaliz(Value);
  if not TDirectory.Exists(Value) then
    TDirectory.CreateDirectory(Value);
end;

function TWebCash.UrlToFileName(const AUrl: string): string;
begin
  Result := TPath.Combine(Path, AUrl.Replace(':', '').Replace('/', '_'));
end;

{ TPathX }

class function TPathX.Normaliz(const APath: string): string;
begin
  Result := APath.Replace('\', TPath.DirectorySeparatorChar).Replace('/', TPath.DirectorySeparatorChar);
end;

end.

